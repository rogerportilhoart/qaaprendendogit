package com.montanha.gerenciador.viagens;

import io.restassured.http.ContentType;
import org.junit.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class ViagensTest {
    @Test
    public void testDadoUmAdministradorQuandoCadastroViagensEntaoObetenhoStatusCode201() {
        //Primeiro configurar o caminho comum de acesso a minha API Rest;
        baseURI = "http://localhost";
        port = 8089;
        basePath = "/api";

        //Segundo, fazer o login na API Rest com administrador;
        String token = given()
                .body("{\n" +
                        " \"email\": \"admin@email.com\", \n" +
                        " \"senha\": \"654321\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .when()
                .post("/v1/auth")
                .then()
                //.log().all()
                .extract()
                .path("data.token");

        //System.out.println(token);

        //Terceiro, vou precisar cadastrar a viagem;
        given()
                .header("Authorization", token)
                .body("{\n" +
                        " \"acompanhante\": \"Isabela\", \n" +
                        " \"dataPartida\": \"2021-03-15\", \n" +
                        " \"dataRetorno\": \"2021-04-15\", \n" +
                        " \"localDeDestino\": \"Manaus\", \n" +
                        " \"regiao\": \"Norte\"\n" +
                        "}")
                //o melhor aqui seria construir uam classe em JAVA simples, com os 5 atributos e setters e getters para todos eles.

                .contentType(ContentType.JSON)
                .when()
                .post("/v1/viagens")
                .then()
                .log().all()
                .assertThat()
                .statusCode(201);

    }
}
